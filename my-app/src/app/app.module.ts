import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { GetbuttonComponent } from './getbutton/getbutton/.component';
import { PostbuttonComponent } from './postbutton/postbutton.component';

@NgModule({
  declarations: [
    AppComponent,
	GetbuttonComponent,
    PostbuttonComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: 'getbutton', component: GetbuttonComponent},
      {path: 'postbutton', component: PostbuttonComponent},
      {path: '', redirectTo: '/postbutton', pathMatch: 'full'},
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
